package com.isitcom.nuage.services.impl;

import com.isitcom.nuage.dto.RescueEventDto;
import com.isitcom.nuage.repositories.dao.RescueEventDao;
import com.isitcom.nuage.repositories.dao.entities.RescueEvent;
import com.isitcom.nuage.services.RescueEventService;
import com.isitcom.nuage.services.mapper.ApplicationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Service
public class RescueEventServiceImpl implements RescueEventService {

    @Autowired
    private RescueEventDao rescueEventDao;

    @Autowired
    private ApplicationMapper applicationMapper;

    @Override
    public void save(RescueEventDto rescueEventDto) {
        rescueEventDao.save(applicationMapper.convert(rescueEventDto, RescueEvent.class));
    }

    @Override
    public List<RescueEventDto> list() {
        return applicationMapper.convertList(rescueEventDao.list(),RescueEventDto.class);
    }
}
