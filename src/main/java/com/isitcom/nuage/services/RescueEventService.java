package com.isitcom.nuage.services;

import com.isitcom.nuage.dto.RescueEventDto;

import java.util.List;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
public interface RescueEventService {

    void save(RescueEventDto rescueEventDto);

    List<RescueEventDto> list();
}
