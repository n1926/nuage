package com.isitcom.nuage.controller;

import com.isitcom.nuage.dto.RescueEventDto;
import com.isitcom.nuage.repositories.dao.entities.RescueEvent;
import com.isitcom.nuage.services.RescueEventService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Controller
public class RescueEventController {

    @Autowired
    private RescueEventService rescueEventService;

    @Operation(description = "Add new rescue event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Event recorded",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid destination data",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Business exception",
                    content = @Content)})
    @PostMapping("/post-rescue-event")
    public ResponseEntity<String> addDestination(@RequestBody RescueEventDto rescueEventDto) {
        rescueEventService.save(rescueEventDto);
        return new ResponseEntity<>("", HttpStatus.CREATED);
    }

    @Operation(description = "Add new rescue event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Event recorded",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid destination data",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Business exception",
                    content = @Content)})
    @PostMapping("/validate")
    public ResponseEntity<String> validate(@RequestBody RescueEventDto rescueEventDto) {
        rescueEventService.save(rescueEventDto);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @Operation(description = "Add new rescue event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Event recorded",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid destination data",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Business exception",
                    content = @Content)})
    @PostMapping("/list/all")
    public ResponseEntity<String> list() {
        rescueEventService.list();
        return new ResponseEntity<>("", HttpStatus.OK);
    }

}
