package com.isitcom.nuage.repositories.dao.impl;

import com.isitcom.nuage.dto.RescueEventDto;
import com.isitcom.nuage.repositories.RescueEventRepository;
import com.isitcom.nuage.repositories.dao.RescueEventDao;
import com.isitcom.nuage.repositories.dao.entities.RescueEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Component
public class RescueEventDaoImpl implements RescueEventDao {

    @Autowired
    private RescueEventRepository rescueEventRepository;

    @Override
    public void save(RescueEvent rescueEvent) {
        rescueEventRepository.save(rescueEvent);
    }

    @Override
    public List<RescueEvent> list() {
        return rescueEventRepository.findAll();
    }
}
