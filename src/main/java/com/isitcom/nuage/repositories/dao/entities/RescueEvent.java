package com.isitcom.nuage.repositories.dao.entities;

import com.isitcom.nuage.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RescueEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    private String visitorName;

    @NotBlank
    @Size(max = 20)
    private String visitorLastName;

    @NotBlank
    @Size(max = 10)
    private String phoneNumber;

    @NotBlank
    @Size(max = 50)
    @Email
    private String visitorEmail;

    @Size(max = 50)
    private String rescueeName;

    @Size(max = 50)
    private String rescueeLastName;

    private LocalDate dateNaissance;

    @Lob
    private String message;

    private StatusEnum statusEnum;


}
