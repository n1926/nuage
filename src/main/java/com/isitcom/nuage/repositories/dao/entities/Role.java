package com.isitcom.nuage.repositories.dao.entities;

import com.isitcom.nuage.enums.ERole;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by mohamed.iben-el-abed on 12/2/2021
 */
@Entity
@Data
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

}
