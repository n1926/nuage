package com.isitcom.nuage.repositories.dao.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */

@Entity
@Data
public class Rescuee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    private String rescueeName;

    @Size(max = 50)
    private String rescueeLastName;

    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL)
    private List<FileJoint> files;

}
