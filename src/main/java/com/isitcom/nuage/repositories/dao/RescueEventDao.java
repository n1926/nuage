package com.isitcom.nuage.repositories.dao;

import com.isitcom.nuage.dto.RescueEventDto;
import com.isitcom.nuage.repositories.dao.entities.RescueEvent;

import java.util.List;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
public interface RescueEventDao {
    void save(RescueEvent rescueEvent);

    List<RescueEvent> list();
}
