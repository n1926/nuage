package com.isitcom.nuage.repositories.dao.entities;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Entity
@Data
public class FileJoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private byte[] file;

}
