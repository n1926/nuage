package com.isitcom.nuage.repositories;

import com.isitcom.nuage.enums.ERole;
import com.isitcom.nuage.repositories.dao.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by mohamed.iben-el-abed on 12/2/2021
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}