package com.isitcom.nuage.repositories;

import com.isitcom.nuage.repositories.dao.entities.RescueEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Repository
public interface RescueEventRepository extends JpaRepository<RescueEvent, Long> {
}
