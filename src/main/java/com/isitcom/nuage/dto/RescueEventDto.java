package com.isitcom.nuage.dto;

import com.isitcom.nuage.enums.StatusEnum;
import lombok.Data;

import java.time.LocalDate;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
@Data
public class RescueEventDto {

    private Long id;

    private String visitorName;

    private String visitorLastName;

    private String phoneNumber;

    private String visitorEmail;

    private String rescueeName;

    private String rescueeLastName;

    private LocalDate dateNaissance;

    private String message;

    private StatusEnum statusEnum;

}
