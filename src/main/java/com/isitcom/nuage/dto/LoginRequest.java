package com.isitcom.nuage.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created by mohamed.iben-el-abed on 12/2/2021
 */
@Data
public class LoginRequest {
    @NotBlank
    private String username;

    @NotBlank
    private String password;

}