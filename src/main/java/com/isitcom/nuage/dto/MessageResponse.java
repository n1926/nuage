package com.isitcom.nuage.dto;

/**
 * Created by mohamed.iben-el-abed on 12/2/2021
 */

public class MessageResponse {
    private String message;

    public MessageResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}