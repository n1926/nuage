package com.isitcom.nuage.enums;

/**
 * Created by mohamed.iben-el-abed on 12/3/2021
 */
public enum StatusEnum {
    RESCUED,
    PENDING,
    REJECTED

}
