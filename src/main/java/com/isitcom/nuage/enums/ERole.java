package com.isitcom.nuage.enums;

/**
 * Created by mohamed.iben-el-abed on 12/2/2021
 */
public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}